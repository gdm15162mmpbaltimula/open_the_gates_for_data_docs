* **OLOD:** NMDAD-I
* **Naam:** Altin Mulaj
* **Groep:** 2MMP proDUCEb

Studa, is een webapplicatie die gebruik maakt van datasets van data.stad.gent, het is een applicatie
bedoeld voor studenten die van het buitenland komen om te studeren in Gent. Voor hun is Gent
nog nieuw en met deze app kunnen studenten terwijl ze in Gent zijn Gent ontdekken en genieten
van hun vrije tijd. De app toont Sportlocaties, Events, Gentste Feesten, Parkeerlocaties en het weer.
Om leuke weetjes over Gent te weten is er ook een daily-Quiz, elke dag is er een nieuwe vraag over
Gent met 4 mogelijkheden om te antwoorden (a,b,c of d).