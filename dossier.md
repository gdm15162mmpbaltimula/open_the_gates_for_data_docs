Dossier Studa
================================
***
Briefing & Analyse
----------
- Indeëenborden + keuze uit ideëenborden resulterend in een Moodboard
    * 3x groep van 3 studenten + finaal 1 resulterende Moodboard

- Sitemap
- Wireframes
    * minimaal 9x groep van 3 studenten

- Style Tiles
    * 3x groep van 3 studenten + finaal 1 resulterende style tile

- Visual designs
    * 3x groep van 3 studenten + finaal 1 resulterende visual design

- Screenshots eindresultaat
    * minimaal 9x groep van 3 studenten

- Screenshots snippets HTML (1x), CSS (1x) en JavaScript (3x)
    * 9x groep van 3 studenten

Functionele specificaties
----------
* Find countries
* See the statistics of each countries
* Compare with other countries

Persona's
----------
###Louis Vanderkercke
![LouisVanderkercke](http://s3-us-west-2.amazonaws.com/s.cdpn.io/155744/profile/profile-512_4.jpg width="200" height="200" "Louis Vanderkercke")

|        |      |
| :-------- | :--------|
| Leeftijd:  | 36 |
| Woonplaats:|   Canada, United States |
| Interesses:|    fitness,wellness,technology |
| Occupation:|    Researcher |
| Onderwijs:|    Hogeschool |
| Telefoon:|   Acer Liquid Z220 |
| Telefoongebruik:|    Extreem |

####Motivations:
Louis Vanderkercke works as a researcher at the University Toronto.<br>
He is specialized in the research of fertility and communicable<br>
child diseases.

####Goals:
* Gathering reports, to find a connection between fertility in industrial<br>
countries and in third world countries.
* To find solutions to stabalize the difference between the fertility
rates.


***

###Ellen Bont
![EllenBont](http://gravatar.com/avatar/0a3a0638ac2aa021799a689db8c17895?s=512 width="200" height="200" "Ellen Bont")

|        |      |
| :-------- | :--------|
| Leeftijd:  | 30 |
| Woonplaats:|  London, United Kingdom |
| Occupation:|    Politician |
| Interesses:|   health, enviroment, agriculture |
| Onderwijs:|    University of Oxford |
| Telefoon:|   iPhone 6 |
| Telefoongebruik:|    Extreem |

####Motivations:
Ellen works as an representative for minister Dufour, at the
department of enviroment and health.

####Goals:
* Gathering data, results, graphics about the mortality rate of
childeren, in dierent countries in Europe.
* Filterering the most accurate information to write a report
for the minister concerning the dierent mortality and
birthrates in Europe.

***

###Sofie Klepke
![SofieKlepke](http://s3-us-west-2.amazonaws.com/s.cdpn.io/240947/profile/profile-512_1.jpg width="200" height="200" "Sofie Klepke")

|        |      |
| :-------- | :--------|
| Leeftijd:  | Canada, United States |
| Woonplaats:|   36 |
| Occupation:|    Researcher |
| Interesses:|    fitness,wellness,technology |
| Onderwijs:|    Hogeschool |
| Telefoon:|   Acer Liquid Z220 |
| Telefoongebruik:|    Extreem |

####Motivations:
Sophie is currently working as a product manager for a company that makes agricultural machinery. Will soon open a new production itself out-division in Brazil, Sofie got a job offer in the new production Section I

####Goals:
Sophie will have children and aims to determine whether Brazile has a good health-care option for babies and mothers.




Indeëenborden
----------
[Indeëenborden 1](https://www.dropbox.com/s/vm3fkny3on585ut/Moodboard1.pdf?dl=0)<br>
[Indeëenborden 2](https://www.dropbox.com/s/1iqw9nss4lyhl4t/moodboard2.pdf?dl=0)<br>
[Indeëenborden 3](https://www.dropbox.com/s/81hztoi6ocwfps0/moodboard3.pdf?dl=0)

Moodboard
----------
[Moodboard](https://www.dropbox.com/s/ksnlalmj9tfet3v/Final%20Moodboard.pdf?dl=0)

Sitemap
----------
[Sitemap](https://www.dropbox.com/s/ju7zhhw5e21bpk9/Sitemap.pdf?dl=0)

Wireframes
----------
[Wireframes](https://www.dropbox.com/s/oap0uon3zxu9kzf/wireframes.pdf?dl=0)

Style Tiles
----------
[Style Tiles 1](https://www.dropbox.com/s/c1mc5u7lcvrdktb/styletile1.pdf?dl=0)<br>
[Style Tiles 2](https://www.dropbox.com/s/p45o66cuayk4cxb/styletile2.pdf?dl=0)<br>
[Style Tiles 3](https://www.dropbox.com/s/t4o88h8et5ce5k5/styletile3.pdf?dl=0)<br>
[Style Tiles Final](https://www.dropbox.com/s/m14aom6n0qhnjei/styletile%20FINAL.pdf?dl=0)

Visual designs
----------
[Visual designs](http://)


Screenshots (eindresultaat)
----------
[Screenshots](http://)

Screenshots (HTML, CSS en JavaScript)  
----------
[Screenshots](http://)

Tijdsbesteding
----------
[Tijdsbesteding](https://studentarteveldehsbe-my.sharepoint.com/personal/altimula_student_arteveldehs_be/_layouts/15/guestaccess.aspx?guestaccesstoken=leWSau0gyX9gDDliTnhPKl0EqBrLiYmXvnuV2HurNhw%3d&docid=2_184fdde6c8fc84dfc8ee8fba05ff70719)